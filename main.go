package main

import (
	"fmt"
	"image/color"
	"log"
	"math"
	"runtime"
	"sync"
	"time"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	pb "gopkg.in/cheggaaa/pb.v1"
)

type (
	particle struct {
		x      float64
		y      float64
		charge float64
		long   float64
	}

	environment struct {
		particlesInPlate      int
		particlesPerMeter     float64
		distanceBetweenPlates float64
		chargePerParticle     float64
		observerCharge        float64
	}
	prefix struct {
		factor         float64
		representation string
	}
)

const (
	f = 1 / (4 * math.Pi * (0.000000000008854187817))
)

var (
	bar    *pb.ProgressBar
	mutexi = make([]sync.Mutex, runtime.NumCPU())
	wg     sync.WaitGroup
	micro  = prefix{
		factor:         math.Pow10(-6),
		representation: "µ",
	}
	distancePrefix = micro
)

func main() {
	var environment = &environment{}
	var err error
	fmt.Print("Please enter the amount of particles in half of one plate (amount of particles): ")
	_, err = fmt.Scanf("%d", &environment.particlesInPlate)
	if err != nil {
		log.Fatal("could not parse amount of particles in half of one plate: ", err.Error())
	}

	fmt.Print("How many particles per meter in a plate? (amount of particles): ")
	_, err = fmt.Scanf("%f", &environment.particlesPerMeter)
	if err != nil {
		log.Fatal("could not parse particles per meter in a plate: ", err.Error())
	}

	fmt.Print("What is the distance between the two plates? (" + distancePrefix.ToString() + "): ")
	_, err = fmt.Scanf("%f", &environment.distanceBetweenPlates)
	if err != nil {
		log.Fatal("could not parse distance between plates: ", err.Error())
	}

	fmt.Print("What is the charge per particle? (e): ")
	_, err = fmt.Scanf("%f", &environment.chargePerParticle)
	if err != nil {
		log.Fatal("could not parse charge per particle: ", err.Error())
	}
	environment.chargePerParticle = environment.chargePerParticle * 1.602176565 * math.Pow10(-19)
	fmt.Print("What is the charge of the observer? (e): ")
	_, err = fmt.Scanf("%f", &environment.observerCharge)
	if err != nil {
		log.Fatal("could not parse charge of the observer: ", err.Error())
	}

	environment.observerCharge = environment.observerCharge * 1.602176565 * math.Pow10(-19)

	distanceBetweenPlatesCM := int(environment.distanceBetweenPlates - 1) // -1 is to not calculate at r=0 , this wil make us devide by 0

	bar = pb.New((environment.particlesInPlate * distanceBetweenPlatesCM))
	bar.ShowTimeLeft = true
	bar.ShowSpeed = true
	bar.SetRefreshRate(200 * time.Millisecond)
	bar.Start()

	topPlateForces := make(plotter.XYs, distanceBetweenPlatesCM)
	bottomPlateForces := make(plotter.XYs, distanceBetweenPlatesCM)
	combinedForces := make(plotter.XYs, distanceBetweenPlatesCM)

	forces := make([]float64, distanceBetweenPlatesCM)
	for layer := 0; layer < distanceBetweenPlatesCM; layer++ {
		mutexi[layer%len(mutexi)].Lock()
		wg.Add(1)

		go func(layer int) {
			forces[layer] = environment.calcPlate(distancePrefix.toStandart(float64(layer+1))) * environment.observerCharge
			topPlateForces[layer].X = float64(layer)
			topPlateForces[layer].Y = forces[layer]
			bottomPlateForces[distanceBetweenPlatesCM-layer-1].X = float64(float64(distanceBetweenPlatesCM) - float64(layer) - 1)
			bottomPlateForces[distanceBetweenPlatesCM-layer-1].Y = forces[layer]
			mutexi[layer%len(mutexi)].Unlock()
			wg.Done()
		}(layer)
	}
	wg.Wait()

	for layer := 0; layer < distanceBetweenPlatesCM; layer++ {
		combinedForces[layer].X = float64(distanceBetweenPlatesCM - layer - 1)
		combinedForces[layer].Y = topPlateForces[layer].Y + bottomPlateForces[layer].Y
	}

	plot, err := plot.New()
	if err != nil {
		panic(err)
	}

	plot.Title.Text = "Charge on a homogenous plane between two pltes"
	plot.X.Label.Text = "Distance from top plate (cm)"
	plot.Y.Label.Text = "Force applied on observer (newton)"

	topPlateLine, err := plotter.NewLine(topPlateForces)
	bottomPlateLine, err := plotter.NewLine(bottomPlateForces)
	combinedLine, err := plotter.NewLine(combinedForces)

	topPlateLine.LineStyle.Color = color.RGBA{R: 255, A: 255}
	topPlateLine.LineStyle.Dashes = []vg.Length{vg.Points(1)}
	bottomPlateLine.LineStyle.Color = color.RGBA{G: 255, A: 255}
	bottomPlateLine.LineStyle.Dashes = []vg.Length{vg.Points(1)}
	combinedLine.LineStyle.Color = color.RGBA{B: 255, A: 255}
	combinedLine.LineStyle.Dashes = []vg.Length{vg.Points(1)}

	t, _ := plotter.NewLine(topPlateLine)
	b, _ := plotter.NewLine(bottomPlateLine)
	c, _ := plotter.NewLine(combinedForces)

	t.LineStyle.Color = color.RGBA{R: 255, A: 255}
	t.LineStyle.Width = vg.Points(1)
	t.LineStyle.Dashes = []vg.Length{vg.Points(1)}

	b.LineStyle.Color = color.RGBA{G: 255, A: 255}
	b.LineStyle.Width = vg.Points(1)
	b.LineStyle.Dashes = []vg.Length{vg.Points(1)}

	c.LineStyle.Color = color.RGBA{B: 255, A: 255}
	c.LineStyle.Width = vg.Points(1)
	c.LineStyle.Dashes = []vg.Length{vg.Points(1)}

	plot.Add(t, c)
	plot.Add(bottomPlateLine)
	plot.Add(combinedLine)

	plot.Legend.Add("Force from top plate", topPlateLine)
	plot.Legend.Add("Force from bottom plate plate", bottomPlateLine)
	plot.Legend.Add("Combined force", combinedLine)

	// Save the plot to a PNG file.
	if err := plot.Save(20*vg.Centimeter, 20*vg.Centimeter, "points.svg"); err != nil {
		panic(err)
	}
}

func (p *particle) calculateForce() (force float64) {
	//fmt.Println(p.long)
	E := f * (p.charge / math.Pow(p.long, 2))
	return E
}

func (p *particle) calculateAjustedForce(force float64) (ajustedForce float64) {
	ajustedForce = (p.y / p.long) * force * 2
	return
}

func (env *environment) calcPlate(yAxis float64) (totalForcePerColoumb float64) {
	p := particle{
		y:      yAxis,
		charge: env.chargePerParticle,
	}

	for i := 0; i < env.particlesInPlate; i++ {
		p.x = float64(i) / env.particlesPerMeter
		p.long = math.Sqrt(math.Pow(float64(i)/env.particlesPerMeter, 2) + math.Pow(yAxis, 2))
		force := p.calculateForce()
		totalForcePerColoumb += p.calculateAjustedForce(force)
		bar.Increment()
	}
	return
}

func (pr *prefix) toStandart(amount float64) float64 {
	return amount * float64(pr.factor)
}

func (pr *prefix) toPrefix(amount float64) float64 {
	return amount / float64(pr.factor)
}

func (pr *prefix) ToString() string {
	return pr.representation
}

module gitlab.com/creativeguy2013/emfield-plate-calculator

require (
	github.com/Workiva/go-datastructures v1.0.50
	github.com/mattn/go-runewidth v0.0.3 // indirect
	golang.org/x/sys v0.0.0-20180906133057-8cf3aee42992 // indirect
	gonum.org/v1/plot v0.0.0-20180905080458-5f3c436ce602
	gopkg.in/cheggaaa/pb.v1 v1.0.25
)
